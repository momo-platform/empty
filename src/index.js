import { View } from "react-native";

const CameraKitGallery = () => {
  return <View />;
};

const CameraKitCamera = () => {
  return <View />;
};

const CameraKitGalleryView = () => {
  return <View />;
};

const CameraKitCameraScreen = () => {
  return <View />;
};

export {
  CameraKitGallery,
  CameraKitCamera,
  CameraKitGalleryView,
  CameraKitCameraScreen,
};
